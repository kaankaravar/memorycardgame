package com.example.memorycardgame;

import android.os.SystemClock;
import android.widget.Chronometer;

public class Kronometre {

    Chronometer kronometre;

    Boolean run = false;
    long time;

    kronometre =  findViewById(R.id.kronometre);

// kronometre find protected void onCreate(Bundle savedInstanceState) kısmına yazılacak

    public void StartKronometre()      // her oyun başladığında bu kısım çalışacak
    {
        if (!run) {
            kronometre.setBase(SystemClock.elapsedRealtime() - time);
            kronometre.start();
            run = true;
        }
    }


    public void StopKronometre()    // hak bittiğinde veya oyun bittiğinde bu kısım çalışacak
    {
        if (run) {
            kronometre.stop();
            time = SystemClock.elapsedRealtime() - kronometre.getBase();
            run = false;
        }
    }


    public void ResetKronometre()    // kronometre'nin baştan başlaması için bu kısım çalışacak
    {
        kronometre.setBase(SystemClock.elapsedRealtime());
        time = 0;


    }
}